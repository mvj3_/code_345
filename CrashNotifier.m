//BSCrashNotifier is a bundle that allows you to be notified when your app is crashing. 
NSBundle* bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"BSCrashNotifier" ofType:@"bundle"]]; 
Class crashNotifierClass = [bundle principalClass]; 
if (crashNotifierClass) 
{ 
    [crashNotifierClass onCrashSend:@selector(weCrashed:) to:self]; 
} 
else
{ 
    NSLog(@"couldn't load bundle"); 
} 
 
//and implement the notification method like: 
- (void)weCrashed:(int)signalNumber 
{ 
    NSLog(@"we crashed: %i",signalNumber); 
} 
